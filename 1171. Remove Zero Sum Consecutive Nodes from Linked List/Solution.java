class Solution {
    public ListNode removeZeroSumSublists(ListNode head) {
        if(head==null)
            return null;
        ListNode currNode=head;
        int currSum=0;
        while(currNode!=null){
            currSum+=currNode.val;
            if(currSum==0){
                head=currNode.next;
                return removeZeroSumSublists(head);
            }
            currNode=currNode.next;
        }
        head.next=removeZeroSumSublists(head.next);
        return head;
    }
}